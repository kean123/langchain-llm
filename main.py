# coding: utf-8
from langchain.llms import OpenAI
from langchain.chat_models import ChatOpenAI
from langchain.callbacks.streaming_stdout import StreamingStdOutCallbackHandler
from langchain.schema import (
    HumanMessage,
)

openai_api_base = "http://www.xiuwen.work:6002/v1"
openai_api_key = "test"

# /v1/chat/completions流式响应
chat_model = ChatOpenAI(streaming=True, callbacks=[StreamingStdOutCallbackHandler()], openai_api_base=openai_api_base, openai_api_key=openai_api_key)
resp = chat_model([HumanMessage(content="给我一个django admin的demo代码")])
chat_model.predict("你叫什么?")

# /v1/chat/completions普通响应
chat_model = ChatOpenAI(openai_api_base=openai_api_base, openai_api_key=openai_api_key)
resp = chat_model.predict("给我一个django admin的demo代码")
print(resp)

# /v1/completions流式响应
llm = OpenAI(streaming=True, callbacks=[StreamingStdOutCallbackHandler()], temperature=0, openai_api_base=openai_api_base, openai_api_key=openai_api_key)
llm("登鹳雀楼->王之涣\n夜雨寄北->")

# /v1/completions普通响应
llm = OpenAI(openai_api_base=openai_api_base, openai_api_key=openai_api_key)
print(llm("登鹳雀楼->王之涣\n夜雨寄北->"))